#!/bin/bash

# Helper function to draw a box.
# Source: https://unix.stackexchange.com/a/70616
function box_out() {
  local s=("$@") b w
  for l in "${s[@]}"; do
    ((w < ${#l})) && {
      b="$l"
      w="${#l}"
    }
  done
  tput setaf 3
  echo " -${b//?/-}-
| ${b//?/ } |"
  for l in "${s[@]}"; do
    echo "$l" | while IFS= read -r line; do
      printf '| %s%*s%s |\n' "$(tput setaf 4)" "-$w" "$line" "$(tput setaf 3)"
    done
  done
  echo "| ${b//?/ } |
 -${b//?/-}-"
  tput sgr 0
}
