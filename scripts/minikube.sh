#!/bin/bash

PROFILE="${2:-adjust-camilo}"
KUBERNETES_VERSION="v1.23.3"

# This status function is not meant to be stable. We don't catch the case were minikube gives us
# an error back.
function status() {
  status=$(minikube --profile=${PROFILE} status -o json | jq '.[0] | select(.Host=="Running" and .Kubelet=="Running" and .APIServer=="Running")')
  if [[ ${#status} -gt 0 ]]; then
    echo "Running"
    exit 0
  else
    echo "Not running.."
    exit 1
  fi
}

# start() will startup minikube with a profile applied.
function start() {
  minikube start \
    --addons ingress \
    --addons registry \
    --bootstrapper=kubeadm \
    --container-runtime=docker \
    --embed-certs \
    --extra-config=controller-manager.bind-address=0.0.0.0 \
    --extra-config=controller-manager.horizontal-pod-autoscaler-downscale-delay=40s \
    --extra-config=controller-manager.horizontal-pod-autoscaler-downscale-stabilization=40s \
    --extra-config=controller-manager.horizontal-pod-autoscaler-sync-period=10s \
    --extra-config=controller-manager.horizontal-pod-autoscaler-upscale-delay=40s \
    --extra-config=kubelet.authentication-token-webhook=true \
    --extra-config=kubelet.authorization-mode=Webhook \
    --extra-config=scheduler.bind-address=0.0.0.0 \
    --kubernetes-version="${KUBERNETES_VERSION}" \
    --nodes 3 \
    --profile ${PROFILE} \
    --wait="all"
}

# stop() will stop the minikube cluster with a profile applied.
function stop() {
  minikube stop \
    --profile ${PROFILE}
}

case "$1" in
start) start ;;
status) status ;;
stop) stop ;;
*)
  echo "usage: $0 start|stop|status" >&2
  exit 1
  ;;
esac
