# Application

**Scenario**

The scenario is described in the challenge doc, which can be found here: [Adjust Challenge Senior Platform Engineer (PDF)](docs/Adjust_Challenge_Senior_Platform_Engineer.pdf)

### Answers

---

#### Assumption/Discussion

To solve the task, assumptions must be made beforehand:

1. **Web-server**
   
   1. The response to the GET request may be JSON.
   2. There will be always a web-server pod running. Scaling to 0 is not supported.
   3. There is not need to add for additional start parameters, e.g. --port-number (default is 8090)

2. **Accessing the Services**
   
   In the task it is written that an ingress should be created. The implementation for this is available, but the last step requires  that the user changes his /etc/hosts. I decided to enable the `NodePort` access for all relevant services.

3. **Development environment**
   
   The solution was written on the following system and no attention was paid to platform independence:

```bash
└──● uname -a
Linux xxx 5.18.6-zen1-1-zen #1 ZEN SMP PREEMPT_DYNAMIC Wed, 22 Jun 2022 18:11:50 +0000 x86_64 GNU/Linux
```

```bash
└──● docker version
Client:
 Version:           20.10.17
 API version:       1.41
 Go version:        go1.18.3
 Git commit:        100c70180f
 Built:             Sat Jun 11 23:27:28 2022
 OS/Arch:           linux/amd64
 Context:           default
 Experimental:      true

Server:
 Engine:
  Version:          20.10.17
  API version:      1.41 (minimum version 1.12)
  Go version:       go1.18.3
  Git commit:       a89b84221c
  Built:            Sat Jun 11 23:27:14 2022
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          v1.6.6
  GitCommit:        10c12954828e7c7c9b6e0ea9b0c02b01407d3ae1.m
 runc:
  Version:          1.1.3
  GitCommit:
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
```

```bash
└──● minikube version
minikube version: v1.25.2
commit: 362d5fdc0a3dbee389b3d3f1034e8023e72bd3a7-dirty
```

---

### 1 Demo

#### 1.1 Installation

For simplicity I have created a Makefile. The command `make all` does the following steps:

1. it builds the image for the web server, which will be used in k8s
2. it builds an image which will be used as a "deployer" image. This is image is being used for the deployment. It stabilizes the deployment and ensures that the deployment works under different local system environments.
3. it starts minikube with three nodes and with the necessary configurations.
4. it installs prometheus, prometheus-adapter and the web-server via a helm chart

After the last step an output is given, which can be used to access the web service.

```bash
**************************************************************
* Adjust Homework                                            *
* Author: Camilo Schoeningh                                  *
**************************************************************

all                            This is bringing up the web-server demo
clean                          This will delete the minikube cluster and the docker images
bash                           Runs a bash inside the deployer image
docker-build-images            Build docker containers
docker-push-images             Push docker images to minikube
start-minikube                 Starts minikube
stop-minikube                  Stops minikube
install-webserver              Installs the web-server inside the minikube cluster
install-prometheus-stack       Installs the prometheus stack
remove-docker-images           Removes all docker images
remove-minikube                Removes the minikube cluster (only the application profile)
remove-prometheus-stack        Removes the prometheus stack
remove-webserver               Uninstalls the web-server
info                           Outputs the cluster and app information
```

```bash
# Start the cluster and webserver
foo@bar:~$ make all

# Get the deployment info
foo@bar:~$ make info
```

#### 1.2 Additional notes

To increase security, all docker images have been prepared to run as non-root. In the deployment the security context was enabled.

#### 1.3 Web-server

The web-server (go-webserver) take requests on to any path via port :8090 and returns a JSON struct. The JSON response contains the host name and the time stamp of the request. There is one path which contains a metric interface. This will be used by the prometheus scraper. The golang code contains a basic unit-test. 

go-webserver is built in a two stage docker build. The image that is deployed has only one binary (scratch image)

![](assets/2022-06-27-23-58-50-image.png)

The web-server is deployed behind a service. The service ensures that requests are shared between the instances.

#### 1.4 Prometheus stack

The web-server exposes a metrics which will be scraped.

To collect the custom metric exported by the web-server. We use Prometheus to scrape. The middleware between prometheus and the custom metric API Endpoint is the [prometheus-adapter](https://github.com/kubernetes-sigs/prometheus-adapter). the adapter asks prometheus for the metric and then registers it at the custom metric endpoint. [ref: config](./helm/prometheus-adapter/values.yaml) 

<div style="text-align: center">
<img src="./assets/scrape-diagram.png" alt="diagram-custom-metrics">
</div>
<div style="text-align: right">
<a href="https://dev.to/mjace/horizontal-pod-autoscale-with-custom-prometheus-metrics-5gem">source dev.to</a>
</div>

The HPA will scale based on the average of this metric: 

```
sum(rate(http_request_total{}[2m]))
```

```bash
└──● kubectl -n webserver get hpa webserver
NAME        REFERENCE              TARGETS     MINPODS   MAXPODS   REPLICAS   AGE
webserver   Deployment/webserver   200m/300m   1         10        2          9m3s
```

#### 1.6 Testing

To test the HPA you can just execute

```bash
WEBSERVER=$(minikube service --profile=adjust-camilo --namespace webserver --url webserver); for i in {1..300}; do curl ${WEBSERVER} > /dev/null 2>&1; done \
&& watch -n 1 -d kubectl -n webserver describe hpa webserver
```

Doing manual web requests can be done by just  accessing the service endpoints:

![](assets/2022-06-28-00-54-24-image.png)
