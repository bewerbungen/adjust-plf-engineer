#!/bin/bash

. /helper_functions.sh

# Set User and print image informations

USER_ID=${LOCAL_USER_ID:-9001}

addgroup -S $USER_ID
adduser -G $USER_ID -u $USER_ID -h $USER_HOME -s /bin/bash -D user
chown -R $USER_ID:$USER_ID $USER_HOME

if [[ ${1} == */bash ]] && [ ${#@} -eq 1 ]; then
  box_out 'Deployer' '' \
    'Author: Camilo Schoeningh' '' \
    "Kubectl version: $(kubectl version --short --client=true 2>/dev/null)" \
    "Helm version: $(helm version --short)" \
    "Gosu version: $(gosu --version)" "" \
    "Current UID: $USER_ID"
fi

exec /usr/local/bin/gosu user "$@"
