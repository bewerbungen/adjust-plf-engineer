#********************************************************************************/
#*The deployer image is used to run kubectl and helm command against the cluster*/
#********************************************************************************/
FROM alpine:3.16

ENV \
	GOSU_VERSION=1.14 \
	HELM_VERSION=3.9.0 \
	KUBE_VERSION=1.24.2 \
	TARGETARCH=amd64 \
	TARGETOS=linux \
	USER_HOME=/home/user

#######################
# Create directories
#######################

RUN mkdir -p \
	/config \
	$USER_HOME

#######################
# Install packages
#######################

RUN set -eux; \
	apk add --no-cache --virtual .gosu-deps \
	gnupg \
	dpkg \
	&& apk add --no-cache \
	bash \
	bind-tools \
	ca-certificates \
	curl \
	gettext \
	git \
	gnupg \
	jq \
	ncurses \
	openssh; \
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
	# Verifiy the signature
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
	command -v gpgconf && gpgconf --kill all || :; \
	rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc \
	# Install kubectl
	&& wget -q https://storage.googleapis.com/kubernetes-release/release/v${KUBE_VERSION}/bin/${TARGETOS}/${TARGETARCH}/kubectl \
	   -O /usr/local/bin/kubectl \
	# Install helm
	&& wget -q https://get.helm.sh/helm-v${HELM_VERSION}-${TARGETOS}-${TARGETARCH}.tar.gz -O - \
	   | tar -xzO ${TARGETOS}-${TARGETARCH}/helm > /usr/local/bin/helm \
	&& chmod +x /usr/local/bin/helm /usr/local/bin/kubectl \
	# Cleanup
	&& apk del --no-network .gosu-deps

#####################################
# Setup permission and execution bits
#####################################

RUN chmod +x \
	/usr/local/bin/gosu \
	&& chmod g+rwx /config /root


#####################################
# Rest
#####################################

COPY /deployer/deployer_entrypoint.sh /usr/local/bin/entrypoint.sh
COPY /scripts/helper_functions.sh /
RUN chmod +x /usr/local/bin/entrypoint.sh

WORKDIR /src

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
