package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	timeFormat = time.RFC1123
	port       = 8090

	httpRequestsTotal = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "Count of all HTTP requests",
	}, []string{"code", "method"})
)

type Response struct {
	Timestamp string
	Hostname  string
}

func logRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

// response returns the hostname and the timestamp to the requester.
func RequestHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		var (
			err       error
			httpError = 500
		)

		reqTime := time.Now().Format(timeFormat)
		host, err := os.Hostname()
		if err != nil {
			http.Error(w, "Can't get Hostname", httpError)
			return
		}

		jsonResp, err := json.Marshal(Response{
			Timestamp: reqTime,
			Hostname:  host,
		})
		if err != nil {
			http.Error(w, "Error building the json response.", httpError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		if _, err = w.Write(jsonResp); err != nil {
			http.Error(w, "Error writing the json response.", httpError)
			return
		}
	})
}

func main() {
	// register prometheus
	r := prometheus.NewRegistry()
	r.MustRegister(httpRequestsTotal)

	http.Handle("/", promhttp.InstrumentHandlerCounter(httpRequestsTotal, RequestHandler()))
	http.Handle("/metrics", promhttp.HandlerFor(r, promhttp.HandlerOpts{}))

	log.Printf("Start listen on port: :%d", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), logRequest(http.DefaultServeMux)); err != nil {
		log.Fatal(err)
	}
}
