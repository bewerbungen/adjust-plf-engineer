package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

func inTimeSpan(now time.Time, check time.Time, seconds int) bool {
	return check.After(now.Add(time.Duration(0-seconds)*time.Second)) &&
		check.Before(now.Add(time.Duration(seconds)*time.Second))
}

func Test_Handler(t *testing.T) {
	var (
		err      error
		jsonResp Response
	)

	// Initialize the recorder and the handler
	handler := RequestHandler()
	recorder := httptest.NewRecorder()

	req, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(recorder, req)

	// parse json
	if err = json.NewDecoder(recorder.Body).Decode(&jsonResp); err != nil {
		t.Fatal("Can't decode json body.")
	}

	t.Run("returns_time_of_request", func(t *testing.T) {
		timeNow := time.Now()
		timeSpanSec := 1

		timeToCheck, err := time.Parse(timeFormat, jsonResp.Timestamp)
		if err != nil {
			t.Fatal("Can't read the timestamp from the json response.")
		}

		if !inTimeSpan(timeNow, timeToCheck, timeSpanSec) {
			t.Fatalf("The returned timestamp is not in the timespan (%d second)", timeSpanSec)
		}
	})

	t.Run("returns_the_instance_hostname", func(t *testing.T) {
		hostName, err := os.Hostname()
		if err != nil {
			t.Fatal("Can't get the hostname.")
		}

		if jsonResp.Hostname != hostName {
			t.Fatalf("Got a wrong hostname: got %v want %v", jsonResp.Hostname, hostName)
		}
	})
}
