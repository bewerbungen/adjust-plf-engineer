SHELL := /bin/bash
PRINT_START = \e[0;33m
PRINT_END = \e[0m

KUBECONFIG ?= ${HOME}/.kube/config
MINIKUBE_PROFILE=adjust-camilo

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.ONESHELL:
.PHONY: help
help:
	@cat <<-EOF
	**************************************************************
	* Adjust Homework                                            *
	* Author: Camilo Schoeningh                                  *
	**************************************************************
	EOF
	@echo
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)


.DEFAULT_GOAL := help

# DOCKER
export DOCKER_BUILDKIT=1

define DEPLOYER_CMD
@if [ ! -d "$(PWD)/.tmp" ]; then \
	mkdir $(PWD)/.tmp; \
fi
@docker run -it --rm \
	-v $(PWD):/src \
	-v $(PWD)/.tmp/config:/home/user/.config \
	-v $(PWD)/.tmp/cache:/home/user/.cache \
	-v $(KUBECONFIG):/home/user/.kube/config \
	-e KUBECONFIG=/home/user/.kube/config \
	-e LOCAL_USER_ID=$(shell id -u ${USER}) \
	--network host \
	deployer:${MINIKUBE_PROFILE}
endef

all: docker-build-images start-minikube docker-push-images install-prometheus-stack install-webserver info ## This is bringing up the webserver demo
clean: remove-minikube remove-docker-images ## This will delete the minikube cluster and the docker images

.PHONY: docker-build-images
.PHONY: docker-push-images
.PHONY: install-prometheus-stack
.PHONY: install-webserver
.PHONY: remove-docker-images
.PHONY: remove-minikube
.PHONY: remove-prometheus-stack
.PHONY: remove-webserver
.PHONY: start-minikube
.PHONY: stop-minikube

# Function to be used to ensure neccessary binaries are installed
check-binary-%:
	@[ $$(type -P "$*") ] || { \
		source scripts/helper_functions.sh; \
		box_out 'Binary missing' '' \
		"$* is NOT in PATH" 1>&2; exit 1; }

# Function to check if image exists
check-image-%:
	@[ $$(docker images -q $*:${MINIKUBE_PROFILE} 2> /dev/null) ] || { \
		echo "Docker image $* is not build yet." 1>&2; exit 1; \
		} 

bash: check-binary-docker check-image-deployer ## Runs a bash inside the deployer image
	@$(DEPLOYER_CMD) /bin/bash 

docker-build-images: check-binary-docker ## Build docker containers
	@echo -e "$(PRINT_START)*** Building images ***$(PRINT_END)"
	@docker build -t deployer:${MINIKUBE_PROFILE} -f deployer/deployer.Dockerfile .
	@docker build -t webserver:${MINIKUBE_PROFILE} -f go-webserver/Dockerfile .

docker-push-images: check-binary-minikube check-binary-docker ## Push docker images to minikube
	@echo -e "$(PRINT_START)*** Pushing images to minikube ***$(PRINT_END)"
	@minikube --profile ${MINIKUBE_PROFILE} ip > /dev/null 2>&1 || $$(echo "Please run make start-minikube first."; exit 1)
	[ "$(shell $(DEPLOYER_CMD) kubectl -n kube-system rollout status -w daemonset registry-proxy)" ] || { \
		echo "Deploying docker registry failed!" 2>&1; exit 1; }
	$(eval KUBECTL_PID=$(shell kubectl port-forward --namespace kube-system service/registry 5000:80 &> /dev/null & echo $$!))
	@trap '[[ $(KUBECTL_PID) ]] && kill "$(KUBECTL_PID)"' EXIT; \
		for image in $$(docker images --format "{{.Repository}}:{{.Tag}}" | grep ${MINIKUBE_PROFILE} | grep -wv ".*/.*$$"); do \
		docker tag $$image 127.0.0.1:5000/$$image; \
		docker push 127.0.0.1:5000/$$image; \
	done
	@echo "Done."

start-minikube: check-binary-minikube ## Starts minikube
	@echo -e "$(PRINT_START)*** Starting minikube ***$(PRINT_END)"
	scripts/minikube.sh start ${MINIKUBE_PROFILE}

stop-minikube: check-binary-minikube ## Stops minikube
	@echo -e "$(PRINT_START)*** Stopping minikube ***$(PRINT_END)"
	scripts/minikube.sh stop ${MINIKUBE_PROFILE}

install-webserver: check-binary-docker check-binary-minikube check-image-deployer check-image-webserver ## Installs the webserver inside the minikube cluster
	@echo -e "$(PRINT_START)*** Installing the webserver to the namespace webserver ***$(PRINT_END)"
	[ "$(shell $(DEPLOYER_CMD) helm status -n monitoring prometheus -o json | jq .info.status 2>&1 )" = "deployed" ] || { \
		echo "Prometheus is not available. Please run 'make install-prometheus-stack' first." 2>&1; exit 1; }
	@$(DEPLOYER_CMD) helm upgrade --install --create-namespace --set image.tag=${MINIKUBE_PROFILE} \
		--wait -n webserver  webserver /src/helm/webserver

install-prometheus-stack: check-binary-docker check-binary-minikube check-image-deployer ## Installs the prometheus stack
	@echo -e "$(PRINT_START)*** Installing prometheus stack ***$(PRINT_END)"
	@$(DEPLOYER_CMD) helm repo add prometheus-community https://prometheus-community.github.io/helm-charts; \
		helm repo update; \
		helm upgrade --install --create-namespace -n monitoring --wait prometheus prometheus-community/kube-prometheus-stack \
		--set alertmanager.enabled=false \
		--set defaultRules.create=false \
		--set grafana.enabled=false \
		--set nodeExporter.enabled=false \
		--set prometheus.prometheusSpec.podMonitorSelectorNilUsesHelmValues=false \
		--set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false \
		--set prometheus.service.type=NodePort
	@echo -e "$(PRINT_START)*** Installing prometheus-adapter ***$(PRINT_END)"
	@$(DEPLOYER_CMD) helm upgrade --install --create-namespace -n monitoring --wait -f helm/prometheus-adapter/values.yaml \
		prometheus-adapter prometheus-community/prometheus-adapter \
		--set prometheus.url=http://prometheus-kube-prometheus-prometheus \
		--set prometheus.port=9090

remove-docker-images: check-binary-docker ## Removes all docker images
	@echo -e "$(PRINT_START)*** Removing docker images ***$(PRINT_END)"
	@docker rmi -f $(shell docker images --format "{{.Repository}}:{{.Tag}}" | grep ${MINIKUBE_PROFILE}) 2>/dev/null || true

remove-minikube: check-binary-minikube ## Removes the minikube cluster (only the application profile)
	@echo -e "$(PRINT_START)*** Removing minikube cluster ${MINIKUBE_PROFILE} ***$(PRINT_END)"
	@scripts/minikube.sh status ${MINIKUBE_PROFILE} > /dev/null 2>&1 && scripts/minikube.sh stop ${MINIKUBE_PROFILE} || true
	minikube delete --profile ${MINIKUBE_PROFILE}

remove-prometheus-stack: check-binary-docker check-image-deployer ## Removes the prometheus stack
	@echo -e "$(PRINT_START)*** Removing the prometheus stack ***$(PRINT_END)"
	@$(DEPLOYER_CMD) helm uninstall -n monitoring prometheus-adapter
	@$(DEPLOYER_CMD) helm uninstall -n monitoring prometheus

remove-webserver: check-binary-docker check-image-deployer ## Uninstalls the webserver
	@echo -e "$(PRINT_START)*** Removing the webserver ***$(PRINT_END)"
	@$(DEPLOYER_CMD) /usr/local/bin/helm uninstall -n webserver webserver || true

info: ## Outputs the cluster and app information
	@$(eval WEBSERVER := $(shell minikube service --profile=${MINIKUBE_PROFILE} --namespace webserver --url webserver))
	@$(eval PROMETHEUS := $(shell minikube service --profile=${MINIKUBE_PROFILE} --namespace monitoring --url prometheus-kube-prometheus-prometheus))
	@$(eval INGRESS := $(shell kubectl get ingress -n webserver -o jsonpath='{.items[0].status.loadBalancer.ingress[0]}' | jq -r .ip))
	@source scripts/helper_functions.sh; \
		box_out \
		'webserver:  $(WEBSERVER)' \
		'metrics:    $(WEBSERVER)/metric' \
		'prometheus: $(PROMETHEUS)' \
		'' \
		'Ingress:' \
		'To use the ingress, execute the command' \
		'echo "$(INGRESS) adjust-webserver.local" >> /etc/hosts'
